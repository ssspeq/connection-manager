#!/usr/bin/env python3.3
# -*- coding: utf-8 -*-
#
#  main.py
#  
#  Copyright 2013 Per Karlsson, pkarl.home@gmail.com
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import beacon
import time
import connectionmanager
import argparse
import ipaddress
import sys
import constants
import tools
run = True
def main():
    parser=argparse.ArgumentParser()
    parser.add_argument('-a', help = 'Network address with mask, X.X.X.X/X', required=True)
    parser.add_argument('-p', help = 'Network port', required=True)
    parser.add_argument('-id', help = 'ID of this node', required=True)
    parser.add_argument('-i', action='store_true', help = 'Run interactive diagnostic mode')
    parser.add_argument('-e', action='store_true', help = 'Print incoming messages')
    args=parser.parse_args()
    hostAddress = args.a[:-3]
    networkAddress = ipaddress.ip_interface(args.a)
    networkAddress = str(networkAddress.network)
    networkAddress, mask = networkAddress.split('/')
    if args.i == True or args.e == True:
        constants.DEBUG = False
    else:constants.DEBUG = True 
    testbeacon = beacon.beacon(args.id,args.p,networkAddress,hostAddress)
    testbeacon.run()
    cm = connectionmanager.connectionManager(args.id, hostAddress, args.p)
    cm.run()
    if args.i == True and args.e is False:
        while run:
            print('1: List nodes')
            print('2: Send message to node')
            print('3: Send message to all nodes')
            print('4: List topics/channels')
            print('5: Create topic')
            print('6: Create channel')
            print('7: Join channel')
            print('8: List channel members')
            print('9: Send message to channel')
            print('10: Get my messages')
            print('11: Delete topic')
            print('12: Delete channel')
            print('13: Leave channel')            
            print('Command?')
            cmd = sys.stdin.readline().rstrip('\n')
            for case in tools.switch(cmd):
                if case('1'):
                    #os.system('cls' if os.name=='nt' else 'clear')
                    nodes = cm.get_all_nodes()
                    for node in nodes:
                        print(node)
                    break
                if case('2'):
                    print('Enter message:')
                    message = sys.stdin.readline().rstrip('\n')
                    print('NodeID:')
                    node_id = sys.stdin.readline().rstrip('\n')
                    cm.send_message_to_node(message, node_id)
                    break
                if case('3'):
                    print('Enter message:')
                    message = sys.stdin.readline().rstrip('\n')
                    cm.send_message_to_all_connected_nodes(message)
                    break
                if case('4'):
                    topics, channels = cm.get_topics_and_channels()
                    print('Topics:')
                    for topic in topics:
                        print(topic)
                    print('Channels:')
                    for channel_id, channel in channels.items():
                        channel_topic = channel.get_topic_name()
                        channel_name = channel.get_channel_name()
                        print('Channel:',channel_name,'with topic:',channel_topic,'and id',channel_id) 
                    break
                if case('5'):
                    print('Enter topic name:')
                    topic_name = sys.stdin.readline().rstrip('\n')                    
                    cm.create_topic(topic_name)
                    break                
                if case('6'):
                    print('Enter topic name:')
                    topic_name = sys.stdin.readline().rstrip('\n')
                    print('Enter channel name:')
                    channel_name = sys.stdin.readline().rstrip('\n')
                    topics = cm.create_channel(topic_name, channel_name)
                    break
                if case('7'):
                    print('Enter channel id:')
                    channel_id = sys.stdin.readline().rstrip('\n')
                    cm.join_channel(channel_id, args.id)
                    break
                if case('8'):
                    print('Enter channel id:')
                    channel_id = sys.stdin.readline().rstrip('\n')
                    nodes = cm.list_channel_members(channel_id)
                    if nodes != None:
                        for node in nodes:
                            print(node) 
                    break                                
                if case('9'):
                    print('Enter channel id:')
                    channel_id = sys.stdin.readline().rstrip('\n')
                    print('Enter message:')
                    message = sys.stdin.readline().rstrip('\n')
                    cm.send_message_to_channel(channel_id, message) 
                    break                     
                if case('10'):
                    messages = cm.get_all_messages()
                    if messages:
                        for message in messages:
                            print(message) 
                    else:
                        print('No new messages')
                    break
                if case('11'):
                    print('Enter topic name:')
                    topic_name = sys.stdin.readline().rstrip('\n')
                    cm.delete_topic(topic_name)
                    break
                if case('12'):
                    print('Enter channel id:')
                    channel_id = sys.stdin.readline().rstrip('\n')
                    cm.delete_channel(channel_id)
                    break
                if case('13'):
                    print('Enter channel id:')
                    channel_id = sys.stdin.readline().rstrip('\n')
                    cm.leave_channel(channel_id, args.id)
                    break
                else:
                    print('You are an idiot')
        
    if args.e == True:
        print('All incoming messages to this node will be printed')
        while True:
            messages = cm.get_all_messages()
            if not messages:
                pass
            else:
                #print('got message')
                for message in messages:
                    print(message)
            time.sleep(0.01)    
    
    else:
        while True:
            time.sleep(1)
    return 0

if __name__ == '__main__':
    main()
