#!/usr/bin/env python3.3
# -*- coding: utf-8 -*-
#
#  channel.py
#  
#  Copyright 2013 Per Karlsson pkarl.home@gmail.com
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from itertools import count
class Channel(object):
    _ids = 0
    def __init__(self, topic_name, channel_name):
        
        self.topic_name = topic_name
        self.channel_name = channel_name
        self.__class__._ids += 1
        self.id = self.__class__._ids
        self.nodes = []
        
    def get_topic_name(self):
        return self.topic_name
    
    def get_channel_name(self):
        return self.channel_name   
    
    def add_node(self, node_id):
        self.node_id = node_id
        self.nodes.append(self.node_id)
        
    def remove_node(self,node_id):
        self.node_id = node_id
        if self.node_id in self.nodes:
            self.nodes.remove(self.node_id)
        
    def get_nodes(self):
        return self.nodes  

    def get_id(self):
        return self.id