#!/usr/bin/env python3.3
# -*- coding: utf-8 -*-
#
#  Node.py
#  
#  Copyright 2013 Per Karlsson, pkarl.home@gmail.com
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

class Node:

    def __init__(self, remote_id, remote_address, remote_port, connection_socket=None):
        self.remote_id = remote_id
        self.remote_address = remote_address
        self.remote_port = remote_port
        self.connection_socket = connection_socket

    def getRemoteId(self):
        return self.__remote_id


    def getRemoteAddress(self):
        return self.__remote_address


    def getRemotePort(self):
        return self.__remote_port

    def getConnectionSocket(self):
        return self.__connection_socket
    
    
    def setRemoteId(self, value):
        self.__remote_id = value


    def setRemoteAddress(self, value):
        self.__remote_address = value


    def setRemotePort(self, value):
        self.__remote_port = value

    def setConnectionSocket(self, value):
        self.__connection_socket = value
        
    def delRemoteId(self):
        del self.__remote_id


    def delRemoteAddress(self):
        del self.__remote_address


    def delRemotePort(self):
        del self.__remote_port

    def delConnectionSocket(self):
        del self.__connection_socket
             

    remote_id = property(getRemoteId, setRemoteId, delRemoteId, "remote_id's docstring")
    remote_address = property(getRemoteAddress, setRemoteAddress, delRemoteAddress, "remote_address's docstring")
    remote_port = property(getRemotePort, setRemotePort, delRemotePort, "remote_port's docstring")
    connection_socket = property(getConnectionSocket, setConnectionSocket, delConnectionSocket, " docstring")