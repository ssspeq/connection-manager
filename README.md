
This is a serverless messagesystem, or maybe you could call it a message broker without a
server. Checkout main.py for an example of how to use it in your python program. Note: It is
likely that it will not work on windows, because it uses epoll.

I made this because I needed something like zeromq, nsq, or something along those lines,
but I could not find anything that was just what I needed, so I decided to make my own version.
This is *NOT* ready, it is in early alpha, the code is a mess, but don't let that stop you
if you want to try it out, it does work, atleast so far as I have tested it. This project is
intended to be used in a distributed security system with no single point of failure, that's
why the repo is named security system, should probably change that.

Run nodes with main.py. Example:

./main.py -a 192.168.150.9/24 -id 11 -p 4506

There are also the flags -e and -i. 
-e will print all incoming messages but nothing else.
-i will present a simple menu.

This project is intended as a messagesystem that has no server. Nodes will find eachother
automatically, the only configuration you need to do is to pass it the ip you will be using
and the netmask, plus an ID (that you have not used for any other node) and after that things
should work. Main.py is just an example of what you could do with this.

Explanation of channels/topics:

A topic is anything you would have the nodes talk about, if you are using this for somekind
of chat, a topic would be easy to understand, let's say, one topic could be the weather.
Now, let's say there are multiple nodes discussing the weather, in different conversations.
This would be your channels. Not that hard, is it? Or, you could create a topic for each node,
and then create message and reply channels, so that you can wait for answers if that is needed
in your use case.

If you want to help or if you have questions:

pkarl.home@gmail.com