#!/usr/bin/env python3.3
# -*- coding: utf-8 -*-
#
#  connectionmanager.py
#  
#  Copyright 2013 Per Karlsson, pkarl.home@gmail.com
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import threading
import socket
import Node
import constants
import time
import tools
import select
import queue
import Channel
import re
s = None
run = True
lock = threading.Lock()
node_dict = {}

outgoing_message_queues = {}#To hold messages to be sent
incoming_message_queue = queue.Queue()
add_to_epoll_queue = queue.Queue()#To temporarily store new nodes that were discovered
my_topics = []  #topic
my_channels = {} #channelid/channel object
handover_socket_to_user_queue = queue.Queue()

class connectionManager: 


    def __init__(self,my_id,my_address,my_port):
        self.my_id = int(my_id)
        self.my_port = int(my_port)
        self.my_address = my_address
        self.serversocket = None
        self.epoll = select.epoll()
        
    def run(self):
        t1 = threading.Thread(target=self.beacon_listener, args=(self.my_id,self.my_address));
        t1.daemon = True
        t1.start()
        t2 = threading.Thread(target=self.socket_handler);
        t2.daemon = True
        t2.start()

    
    def socket_handler(self):
        HOST, PORT = self.my_address, self.my_port
        self.serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.serversocket.bind((HOST, PORT))
        self.serversocket.listen(10)        
        self.connections = {} 
        self.epoll.register(self.serversocket.fileno(), select.EPOLLIN)
        
        while run:
                events = self.epoll.poll(1)
                for fileno, event in events:
             
                    #New connection
                    if fileno == self.serversocket.fileno():
                        self.setup_incoming_connection()
                    #New incoming data
                    elif event & select.EPOLLIN:
                        new_socket = self.connections[fileno]
                        data = tools.recv_msg(new_socket)
                        if data:
                            """This is where we need to start intercepting
                            messages that are not for the incoming queue, but for
                            other things such as topics/channel notifications"""
                            self.handle_incoming_message(data)
                            
                        else:
                            """This socket is broken and will never ever work again
                            so clean up"""
                            self.delete_node(new_socket, fileno)
                    
                    #some socket is writable
                    elif event & select.EPOLLOUT:
                        new_socket = self.connections[fileno]
                        try:
                            next_msg = outgoing_message_queues[new_socket].get_nowait()
                        except queue.Empty:
                            # No messages waiting so stop checking for writability.
                            """Figure out a clever way of making the loop stop notifying us
                            about writable sockets when there is no data in the outqueue."""
                            pass
                            
                        else:
                            if constants.DEBUG:print ('sending "%s" to %s' % (next_msg, new_socket.getpeername()))
                            tools.send_msg(new_socket, next_msg.encode('utf8'))

                    elif event & (select.EPOLLERR | select.EPOLLERR):
                        new_socket = self.connections[fileno]
                        self.delete_node(new_socket, fileno)

                self.add_and_setup_discovered_node()
                time.sleep(0.001)
        
        
        
        
        
        
        #serversocket.close()

    

    def beacon_listener(self,my_id,my_address):
        """Listens for broadcasts from other nodes and places them in epoll queue to be added"""
        
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            s.bind(('0.0.0.0', 6300))
        except:
            print('failure to bind')
            s.close()
            raise
        while run:
            message = s.recv(constants.BUFSIZE)
            message = message.decode('utf8')
            
            beaconid, beaconport, beaconaddress = message.split(' ')
            if constants.DEBUG:
                print('Connectionmanager: Found peer at %r, with id: %r and port: %r' % (beaconaddress, beaconid, beaconport))
            if (int(beaconid) != self.my_id and int(beaconid) < self.my_id and beaconid not in node_dict):
                new_peer = Node.Node(beaconid, beaconaddress, beaconport)
                lock.acquire()
                node_dict.update({beaconid:''})
                lock.release()
                add_to_epoll_queue.put_nowait(new_peer)
                    
                    
        time.sleep(0.01)

    def create_topic(self, topic_name):
        """Creates a new topic, takes string topic_name, returns True on success, false if
        topic was not created because it already existed"""
        
        topic_name = topic_name
        message = tools.make_new_topic_notification_message(topic_name)
        lock.acquire()
        if topic_name not in my_topics:
            my_topics.append(topic_name)
            lock.release()
            self.send_message_to_all_connected_nodes_internal_use(message)
            return True
        else:
            lock.release()
            return False
          
    
    def delete_topic(self, topic_name):
        """Deletes topic, takes string topic_name, returns True on success, False if
        channel was not created for whatever reason"""
        
        lock.acquire()
        if topic_name in my_topics:
            """If there are channels using this topic, this should fail"""
            for channel_id, channel in my_channels.items():
                if channel.get_topic_name() == topic_name:
                    lock.release()
                    return False
            """so it seems no channel uses this topic so lets delete it"""
            my_topics.remove(topic_name)
            """Now let's tell everyone this topic is now a goner"""
            message = tools.make_new_del_topic_notification_message(topic_name)
            lock.release()
            self.send_message_to_all_connected_nodes_internal_use(message)
            return True
        else:
            lock.release()
            return False
    
    def create_channel(self,topic_name, channel_name):
        """Creates channel, takes string topic_topic_name, string channel_name, returns
        True if channel was created, False if it for whatever reason was not"""
        
        #This should raise exception if channel or topic already exists, not just
        #return false, that makes it impossible to know what failed
        topic_name = topic_name
        channel_name = channel_name
        lock.acquire()
        if topic_name not in my_topics:
            my_topics.append(topic_name)
            message = tools.make_new_topic_notification_message(topic_name)
            lock.release()
            self.send_message_to_all_connected_nodes_internal_use(message)
            lock.acquire()
            
        
        for channel_id, channel in my_channels.items():
            if channel_name == channel.get_channel_name() and channel.get_topic_name() == topic_name:
                """This channel exists  for this topic already, return false"""
                if constants.DEBUG:print('this channel exists for this topic')
                return False
        
        new_channel = Channel.Channel(topic_name, channel_name)
        my_channels.update({new_channel.get_id():new_channel})
        lock.release()
        message = tools.make_new_channel_notification_message(channel_name, topic_name)  
        self.send_message_to_all_connected_nodes_internal_use(message)
        return True
            
    def delete_channel(self, channel_id):
        """Removes channel if it has no members, takes string channel_id, returns False if channel
        was not deleted for whatever reason, True if it was"""
        
        lock.acquire()
        channel_id = int(channel_id)
        if channel_id in my_channels.keys():
            channel = my_channels.get(channel_id)
            members = channel.get_nodes()
            if members:
                """People are using this channel, do not delete it"""
                lock.release()
                return False
            else:
                topic_name = channel.get_topic_name()
                channel_name = channel.get_channel_name()
                message = tools.make_new_del_channel_notification_message(topic_name, channel_name)
                lock.release()
                self.send_message_to_all_connected_nodes_internal_use(message)
                del my_channels[channel_id]
                return True
        lock.release()

    def get_topics_and_channels(self):
        """Returns topics list and channel dict"""
        
        lock.acquire()
        tmp_my_topics = my_topics.copy()
        tmp_my_channels = my_channels.copy()
        lock.release()
        return tmp_my_topics, tmp_my_channels 
    
    def join_channel(self,channel_id, my_id):
        """Joins channel specified with string channel_id, also takes string my_id"""
        
        channel_id = int(channel_id)
        my_id = my_id
        lock.acquire()
        channel = my_channels.get(channel_id)
        channel_members = channel.get_nodes()
        if my_id not in channel_members:
            channel.add_node(my_id)
            topic_name = channel.get_topic_name()
            channel_name = channel.get_channel_name()
            lock.release()
            message = tools.make_new_join_channel_notification_message(topic_name, channel_name, my_id)
            self.send_message_to_all_connected_nodes_internal_use(message)
            return True
        else:
            lock.release()
            return False
        
        
    def leave_channel(self, channel_id, my_id):
        """Exit channel specified with string channel_id, string my_id"""
        
        channel_id = int(channel_id)
        my_id = my_id
        lock.acquire()
        channel = my_channels.get(channel_id)
        channel_members = channel.get_nodes()
        if my_id in channel_members:
            channel.remove_node(my_id)
            topic_name = channel.get_topic_name()
            channel_name = channel.get_channel_name()
            lock.release()
            message = tools.make_new_leave_channel_notification_message(topic_name, channel_name, my_id)
            self.send_message_to_all_connected_nodes_internal_use(message)
            return True
        else:
            lock.release()
            return False

    
    def list_channel_members(self, channel_id):
        """Returns channel members list, takes channel id as string"""
        
        lock.acquire()
        if int(channel_id) in my_channels:
            channel = my_channels.get(int(channel_id))
            members = channel.get_nodes()
            lock.release()
            return members
        else:
            lock.release()
            return None
        
        
    def send_message_to_channel(self, channel_id, message):
        """Sends message to channel with channel_id, takes channel_id as string, string message"""
        
        channel_id = int(channel_id)
        message = tools.add_message_header_user(message)
        lock.acquire()
        channel = my_channels.get(channel_id)
        nodes_in_channel = channel.get_nodes()
        for node in nodes_in_channel:
            if node in node_dict:
                tmp_node = node_dict.get(node)
                if tmp_node.getConnectionSocket() in outgoing_message_queues:
                    tmp_queue = outgoing_message_queues.get(tmp_node.getConnectionSocket())
                    tmp_queue.put(message)
                        #print('put data in ', node_id,' queue')
        lock.release()
        pass
    
    def get_channels(self):
        pass
    
    def get_all_nodes(self):
        """Return dict of nodeobjects with key ID"""
        
        lock.acquire()
        tmp_list = node_dict.copy()
        lock.release()
        return tmp_list
    
    def send_message_to_node(self, message, node_id):
        """Send message to connected peer, takes string message, int peerId"""
        
        message = tools.add_message_header_user(message)
        node_id = node_id
        lock.acquire()
        if node_id in node_dict:
            
            tmp_node = node_dict.get(node_id)
            if tmp_node.getConnectionSocket() in outgoing_message_queues:
                tmp_queue = outgoing_message_queues.get(tmp_node.getConnectionSocket())
                tmp_queue.put(message)
                #print('put data in ', node_id,' queue')
        lock.release()
        
    
    def send_message_to_all_connected_nodes(self, message):
        """Sends message to all connected nodes. Takes message as string."""
        
        message = tools.add_message_header_user(message)
        lock.acquire()
        for node in node_dict:
            tmp_node = node_dict.get(node)
            if tmp_node.getConnectionSocket() in outgoing_message_queues:
                tmp_queue = outgoing_message_queues.get(tmp_node.getConnectionSocket())
                tmp_queue.put(message)
                #TODO: Maybe this should be no_wait, and store the message somewhere if
                #outgoing queue for this node is full?
        lock.release()
        
    def send_message_to_all_connected_nodes_internal_use(self, message):
        #TODO: Remove unnecessary duplication of this function
        
        message = message
        lock.acquire()
        for node in node_dict:
            tmp_node = node_dict.get(node)
            if tmp_node.getConnectionSocket() in outgoing_message_queues:
                tmp_queue = outgoing_message_queues.get(tmp_node.getConnectionSocket())
                tmp_queue.put(message)
                #TODO: Maybe this should be no_wait, and store the message somewhere if
                #outgoing queue for this node is full?
        lock.release()

    def get_all_messages(self):
        """Get all messages from all nodes, returns list of messages"""
        
        messages = []
        while 1:
            try:
                messages.append(incoming_message_queue.get_nowait())
            except queue.Empty:
                break
        
        return messages

    
    def delete_node(self, new_socket, fileno):
        """This deletes a node completely, and removes every membership of channels it
        may have had"""
        
        lock.acquire()
        for node in node_dict.keys():
            tmp_node = node_dict[node]
            if tmp_node.getConnectionSocket() is new_socket:
                break
        if constants.DEBUG:print('Node',node,'is dead, removing it and cleaning up')
        del node_dict[node]
        self.epoll.unregister(fileno)
        self.connections[fileno].close()
        del self.connections[fileno]
        for channel_id, channel in my_channels.items():
            channel.remove_node(node)
        lock.release()
    
    
    def add_and_setup_discovered_node(self):
        """This is used by epoll to setup a new node that was discovered by beaconlistener"""
        
        try:
            new_node = add_to_epoll_queue.get_nowait()
        except queue.Empty:
            #TODO: Fix this to handle the case where the queue is full of unadded nodes, it will
            #probably never happen but still
            pass
        else:
            if constants.DEBUG:print('New node to epoll ',new_node.getRemoteId())
                    
            newsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            newsock.connect((new_node.getRemoteAddress(), int(new_node.getRemotePort())))
            newsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            tools.send_msg(newsock, str(self.my_id).encode('utf8'))
            response = tools.recv_msg(newsock)
            if response.decode('utf8') == 'OK':
                if constants.DEBUG:print('Connected to '+new_node.getRemoteId())
                self.send_all_topics_and_channels(newsock)
                newsock.setblocking(0)
                new_node.setConnectionSocket(newsock)
                node_dict.update({new_node.getRemoteId():new_node})
                outgoing_message_queues[newsock] = queue.Queue()
                self.epoll.register(newsock.fileno(), select.EPOLLIN | select.EPOLLOUT | select.POLLERR | select.EPOLLHUP)
                self.connections[newsock.fileno()] = newsock
                if constants.DEBUG:print ("Connectionmanager: New node object with id:"+new_node.getRemoteId()+" and remote_address:"+new_node.getRemoteAddress())
            else:
                if constants.DEBUG:print('Connectionmanager: Node at %r with id %r ignored' % (new_node.getRemoteAddress(), new_node.getRemoteId()))
                

                
    def setup_incoming_connection(self):
        """Epoll loop uses this to setup new nodes that connected to us"""
        
        if constants.DEBUG:print('incoming connection')
        #Node connected to us
        connection, addr = self.serversocket.accept()
        self.epoll.register(connection.fileno(), select.EPOLLIN | select.EPOLLOUT | select.POLLERR | select.EPOLLHUP)
        self.connections[connection.fileno()] = connection
        data = tools.recv_msg(connection)
        connection.setblocking(0)
        node_address = addr[0]
        node_id = data.decode('utf8')
        if node_id == 'interface':
            """This is a graphical client of some sort, hand the socket over
            to be managed by someone else
            """
            handover_socket_to_user_queue.put_nowait(connection)
        else:
            tools.send_msg(connection, b'OK')
            lock.acquire()
            outgoing_message_queues[connection] = queue.Queue()
            new_node = Node.Node(node_id, node_address, 0 ,connection)
            node_dict.update({node_id:new_node})
            lock.release()
            """Update this node with topics and channels"""
            self.send_all_topics_and_channels(connection)
        
            if constants.DEBUG:print("Node (%s, %s) connected" % addr)
            if constants.DEBUG:print('with id',node_id)
        
    def handle_incoming_message(self, data):
        """This is where epoll loop goes with new incoming messages, it checks if the message
        is for some internal function or if it is intended to be delivered to this node"""
        
        data = data.decode('utf8')
        message_type = re.findall(r'<message_type>(.+?)</message_type>',data)[0]
        if message_type == 'user':
            """This is a message to be delivered"""
            data = data.replace("<message_type>user</message_type>","",1)
            try:
                incoming_message_queue.put_nowait(data)
            except queue.Full:
                #In the case where the user does not get his messages, this queue would get full.
                #Maybe implement another storage of messages that are waiting?
                #Also, if we want to keep message arrival order, Im not sure this queue does the
                #job
                pass
            print('got message:',data)
        elif message_type == 'info':
            """This is a notification message"""
            if constants.DEBUG:print('got notification message:',data)
            notification = re.findall(r'<notification>(.+?)</notification>',data)[0]
            if notification == 'new_topic':
                """Someone made a new topic, we must add this to our list"""
                topic = re.findall(r'<topic_name>(.+?)</topic_name>',data)[0]
                if topic not in my_topics:
                    my_topics.append(topic)
                    if constants.DEBUG:print('Added topic:'+topic)
            if notification == 'new_channel':
                """Someone made a new channel, we must add this to our list"""
                if constants.DEBUG:print('got new channel')
                topic_name = re.findall(r'<topic_name>(.+?)</topic_name>',data)[0]
                channel_name = re.findall(r'<channel_name>(.+?)</channel_name>',data)[0]
                if constants.DEBUG:print('got:',topic_name,channel_name)
                channel_already_exists = False
                """If there are no channels, its safe to assume that this channel
                does NOT exist"""
                for channel_id, channel in my_channels.items():
                    if channel.get_topic_name() == topic_name and channel.get_channel_name() == channel_name:
                        """We have this channel already, screw it"""
                        if constants.DEBUG:print('ignoring already known channel')
                        channel_already_exists = True
                if channel_already_exists == False:
                    channel = Channel.Channel(topic_name, channel_name)
                    my_channels.update({channel.get_id(): channel})
                    if constants.DEBUG: print('Added channel:' + channel_name, 'in topic:' + topic_name, 'with local id', channel.get_id())
            
            if notification == 'channel_join':
                if constants.DEBUG:print('Someone joined a channel')
                topic_name = re.findall(r'<topic_name>(.+?)</topic_name>',data)[0]
                channel_name = re.findall(r'<channel_name>(.+?)</channel_name>',data)[0]
                node_id = re.findall(r'<node_id>(.+?)</node_id>',data)[0]
                for channel in my_channels.keys():
                    tmp_channel = my_channels[channel]
                    if tmp_channel.get_channel_name() == channel_name and tmp_channel.get_topic_name():
                        tmp_channel.add_node(node_id)
                        if constants.DEBUG:print('Added node',node_id,'as memmber of channel',channel_name,'in topic',topic_name)
            
            if notification == 'channel_leave':
                if constants.DEBUG:print('Someone left a channel')
                topic_name = re.findall(r'<topic_name>(.+?)</topic_name>',data)[0]
                channel_name = re.findall(r'<channel_name>(.+?)</channel_name>',data)[0]
                node_id = re.findall(r'<node_id>(.+?)</node_id>',data)[0]
                for channel in my_channels.keys():
                    tmp_channel = my_channels[channel]
                    if tmp_channel.get_channel_name() == channel_name and tmp_channel.get_topic_name():
                        members = tmp_channel.get_nodes()
                        if node_id in members:
                            tmp_channel.remove_node(node_id)
                            if constants.DEBUG:print('Removed node',node_id,'as member of channel',channel_name,'in topic',topic_name)
                    else:
                        if constants.DEBUG:print('Node',node_id,'was not in channel',channel_name,'with topic',topic_name)
            
            if notification == 'channel_members':
                topic_name = re.findall(r'<topic_name>(.+?)</topic_name>',data)[0]
                channel_name = re.findall(r'<channel_name>(.+?)</channel_name>',data)[0]
                members = re.findall(r'<members>(.+?)</members>',data)[0]
                members = members.split()
                lock.acquire()
                for channel in my_channels.keys():
                    tmp_channel = my_channels[channel]
                    if tmp_channel.get_channel_name() == channel_name and tmp_channel.get_topic_name():
                        for member in members:
                            tmp_member_list = tmp_channel.get_nodes()
                            if member not in tmp_member_list:
                                tmp_channel.add_node(member)
                                if constants.DEBUG:print('Added node',member,'as member of channel',channel_name,'in topic',topic_name)

                
                lock.release()
            if notification == 'del_topic':
                topic_name = re.findall(r'<topic_name>(.+?)</topic_name>',data)[0]
                lock.acquire()
                if topic_name in my_topics:
                    """We should not be here if any channel uses this topic, but let's check anyway"""
                    for channel_id, channel in my_channels.items():
                        if channel.get_topic_name() == topic_name:
                            return False
                """so it seems no channel uses this topic so lets delete it"""
                my_topics.remove(topic_name)
                lock.release()
            
            if notification == 'del_channel':
                topic_name = re.findall(r'<topic_name>(.+?)</topic_name>',data)[0]
                channel_name = re.findall(r'<channel_name>(.+?)</channel_name>',data)[0]
                lock.acquire()
                tmp_channel = None
                for channel_id, channel in my_channels.items():
                    if channel.get_channel_name() == channel_name and channel.get_topic_name() == topic_name:
                        """This is the channel we want to get rid of now"""
                        tmp_channel = channel.get_id()
                        break
                if tmp_channel:
                    del my_channels[int(tmp_channel)]
                    if constants.DEBUG:print('Deleted channel',channel_name,'with topic',topic_name)
                lock.release()                    
                
    def send_all_topics_and_channels(self, socket):
        """This is used by epoll loop at connection of new node"""
        socket = socket
        lock.acquire()
        for topic in my_topics:
            message = tools.make_new_topic_notification_message(topic)
            tools.send_msg(socket, message.encode('utf8'))
        for channel_id, channel in my_channels.items():
            topic_name = channel.get_topic_name() 
            channel_name = channel.get_channel_name()   
            message = tools.make_new_channel_notification_message(channel_name, topic_name)
            tools.send_msg(socket, message.encode('utf8'))
            members = channel.get_nodes()
            if members:
                message = tools.make_new_update_channel_members_notification_messages(channel_name, topic_name, members)
                tools.send_msg(socket, message.encode('utf8'))
                
        lock.release()
                
    def get_interface_socket(self):
        
        try:
            socket = handover_socket_to_user_queue.get_nowait()
            return socket
        except queue.Empty:
            return False
            
        
        
        
        
        
        