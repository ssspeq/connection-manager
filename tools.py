#!/usr/bin/env python3.3
# -*- coding: utf-8 -*-
#
#  tools.py
#  
#  Copyright 2013 Per Karlsson, pkarl.home@gmail.com
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import struct

COLORRED = "\033[01;31m{0}\033[00m"
COLORGRN = "\033[1;36m{0}\033[00m"


def printGreen(text):

    print (COLORGRN.format(text))

def printRed(text):

    print (COLORRED.format(text))
    
def linesplit(socket):
    buffer = socket.recv(4096)
    done = False
    while not done:
        if "\n" in buffer:
            (line, buffer) = buffer.split("\n", 1)
            yield line+"\n"
        else:
            more = socket.recv(4096)
            if not more:
                done = True
            else:
                buffer = buffer+more
    if buffer:
        yield buffer
    return buffer

class switch(object):
    def __init__(self, value):
        self.value = value
        self.fall = False

    def __iter__(self):
        """Return the match method once, then stop"""
        yield self.match
        raise StopIteration
    
    def match(self, *args):
        """Indicate whether or not to enter a case suite"""
        if self.fall or not args:
            return True
        elif self.value in args: # changed for v1.5, see below
            self.fall = True
            return True
        else:
            return False
        
def send_msg(sock, msg):
    # Prefix each message with a 4-byte length (network byte order)
    msg = struct.pack('>I', len(msg)) + msg
    sock.sendall(msg)

def recv_msg(sock):
    # Read message length and unpack it into an integer
    raw_msglen = recvall(sock, 4)
    if not raw_msglen:
        return None
    msglen = struct.unpack('>I', raw_msglen)[0]
    # Read the message data
    return recvall(sock, msglen)

def recvall(sock, n):
    # Helper function to recv n bytes or return None if EOF is hit
    data = ''
    while len(data) < n:
        packet = sock.recv(n - len(data))
        if not packet:
            return None
        data = data.encode(encoding='utf_8', errors='strict')
        data += packet
    return data

def make_new_topic_notification_message(topic_name):
    topic_name = topic_name
    message ='<message_type>info</message_type><notification>new_topic</notification><topic_name>'+topic_name+'</topic_name>'
    return message
    
def make_new_channel_notification_message(channel_name, topic_name):
    channel_name = channel_name
    topic_name = topic_name
    message = '<message_type>info</message_type><notification>new_channel</notification><topic_name>'+topic_name+'</topic_name><channel_name>'+channel_name+'</channel_name>'
    return message
    
def make_new_join_channel_notification_message(topic_name, channel_name, node_id):
    message = '<message_type>info</message_type><notification>channel_join</notification><topic_name>'+topic_name+'</topic_name><channel_name>'+channel_name+'</channel_name><node_id>'+node_id+'</node_id>'
    return message

def make_new_leave_channel_notification_message(topic_name, channel_name, node_id):
    message = '<message_type>info</message_type><notification>channel_leave</notification><topic_name>'+topic_name+'</topic_name><channel_name>'+channel_name+'</channel_name><node_id>'+node_id+'</node_id>'
    return message    
    
def make_new_update_channel_members_notification_messages(channel_name, topic_name, members):
    message = '<message_type>info</message_type><notification>channel_members</notification><topic_name>'+topic_name+'</topic_name><channel_name>'+channel_name+'</channel_name><members>'
    for member in members:
        message = message+member+' '
    message = message.rstrip()
    message = message+'</members>'
    return message
    
def make_new_del_topic_notification_message(topic_name):
        message ='<message_type>info</message_type><notification>del_topic</notification><topic_name>'+topic_name+'</topic_name>'
        return message

def make_new_del_channel_notification_message(topic_name, channel_name):
        message ='<message_type>info</message_type><notification>del_channel</notification><topic_name>'+topic_name+'</topic_name><channel_name>'+channel_name+'</channel_name>'
        return message    

def add_message_header_user(message):
    message = '<message_type>user</message_type>'+message
    return message